#include <iostream>
#include <stdexcept>
#include <string>
#include <vector>
#include <SFML/Graphics.hpp>
#include <pugixml.hpp>

enum class entity_type_t
{
	wall, food, ghost, pacman, empty
};
class entity_t : public sf::Drawable
{
	public:
		entity_t(sf::Drawable* ptr, entity_type_t type = entity_type_t::wall) :
			body_{ptr}, type_{type}, marked_for_removal_{false} {}
		virtual ~entity_t() { delete body_; }
		entity_t(const entity_t&) = delete;
		auto operator=(const entity_t&) = delete;
		entity_t(entity_t&& other) : body_{other.body_}, type_{other.type_} { other.body_ = nullptr; }
		auto operator=(entity_t&& other) { delete body_; body_ = other.body_; type_ = other.type_; other.body_ = nullptr; }
		//
		sf::Drawable* get() { return body_; }
		entity_type_t type() const { return type_; }
		void mark_for_removal() { marked_for_removal_ = true; }
		bool is_marked_for_removal() const { return marked_for_removal_; }
		virtual void update(std::vector<entity_t*>&) {}
	protected:
		void draw(sf::RenderTarget& target, sf::RenderStates states) const {
			target.draw(*body_, states);
		}
	private:
		sf::Drawable* body_;
		entity_type_t type_;
		bool marked_for_removal_;
};
class square_t : public entity_t
{
	public:
		square_t(float x, sf::Color c, sf::Vector2f p, entity_type_t et) :
		entity_t(new sf::RectangleShape(sf::Vector2f{x,x}), et) {
			dynamic_cast<sf::RectangleShape*>(this->get())->setFillColor(c);
			dynamic_cast<sf::RectangleShape*>(this->get())->setPosition(p);
		}
};
class wall_t : public square_t
{
	public:
		wall_t(sf::Vector2f p) : square_t(10, sf::Color::Blue, p, entity_type_t::wall) {
			++s_count;
		}
		static unsigned get_count() { return s_count; }
	private:
		static inline unsigned s_count;
};
class empty_t : public square_t
{
	public:
		empty_t(sf::Vector2f p) : square_t(10, sf::Color::Black, p, entity_type_t::empty) {
			++s_count;
		}
		static unsigned get_count() { return s_count; }
	private:
		static inline unsigned s_count;
};
class food_t : public square_t
{
	public:
		food_t(sf::Vector2f p) : square_t(10, sf::Color::Green, p, entity_type_t::food) {
			++s_count;
		}
		static unsigned get_count() { return s_count; }
	private:
		static inline unsigned s_count;
};
class ghost_t : public square_t
{
	public:
		enum class dynamic_direction_t { forward = 1, backward = -1 };
		enum class static_direction_t { horizontal, vertical };
		//
		ghost_t(sf::Vector2f a, sf::Vector2f b, short unsigned speed = 1) :
			square_t(10, sf::Color::Red, a, entity_type_t::ghost),
		path_begin_{a}, path_end_{b}, speed_{speed}
		{
			if (auto dif = path_end_.x - path_begin_.x; dif == 0)
				vh_ = static_direction_t::vertical;
			else if (auto dif = path_end_.y - path_begin_.y; dif == 0)
				vh_ = static_direction_t::horizontal;
			++s_count;
		}
		static unsigned get_count() { return s_count; }
		void update(std::vector<entity_t*>&) override {
			auto ptr = dynamic_cast<sf::RectangleShape*>(this->get());
			if (
				(vh_ == static_direction_t::horizontal && ptr->getPosition().x <= path_begin_.x) ||
				(vh_ == static_direction_t::vertical && ptr->getPosition().y <= path_begin_.y)
			) { fb_ = dynamic_direction_t::forward; }
			else if (
				(vh_ == static_direction_t::horizontal && ptr->getPosition().x >= path_end_.x) ||
				(vh_ == static_direction_t::vertical && ptr->getPosition().y >= path_end_.y)
			) { fb_ = dynamic_direction_t::backward; }
			//
			if (vh_ == static_direction_t::vertical)
				ptr->move(0, speed_ * static_cast<short>(fb_));
			else if (vh_ == static_direction_t::horizontal)
				ptr->move(speed_ * static_cast<short>(fb_), 0);
		}
	private:
		sf::Vector2f path_begin_;
		sf::Vector2f path_end_;
		short unsigned speed_;
		dynamic_direction_t fb_;
		static_direction_t vh_;
	private:
		static inline unsigned s_count;
};
class pacman_t : public entity_t
{
	public:
		pacman_t(sf::Vector2f p, short unsigned speed = 1, short lives = 4) : entity_t(new sf::CircleShape(4), entity_type_t::pacman),
		speed_{speed}, lives_{lives}, score_{0} {
			dynamic_cast<sf::CircleShape*>(this->get())->setFillColor(sf::Color::Yellow);
			dynamic_cast<sf::CircleShape*>(this->get())->setPosition(p);
		}
		void update(std::vector<entity_t*>& entities) override {
			auto ptr = dynamic_cast<sf::CircleShape*>(this->get());
			auto old_pos = ptr->getPosition();
			if (sf::Keyboard::isKeyPressed(sf::Keyboard::A)) {
				ptr->move(-speed_, 0);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::D)) {
				ptr->move(speed_, 0);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::W)) {
				ptr->move(0, -speed_);
			}
			else if (sf::Keyboard::isKeyPressed(sf::Keyboard::S)) {
				ptr->move(0, speed_);
			}
			for (auto i = entities.begin(); i != entities.end(); ++i)
			{
				if ((*i)->type() == entity_type_t::wall) {
					auto igb = dynamic_cast<sf::RectangleShape*>((*i)->get())->getGlobalBounds();
					if (ptr->getGlobalBounds().intersects(igb))
						ptr->setPosition(old_pos);
				}
				else if ((*i)->type() == entity_type_t::food) {
					auto igb = dynamic_cast<sf::RectangleShape*>((*i)->get())->getGlobalBounds();
					if (ptr->getGlobalBounds().intersects(igb)) {
						(*i)->mark_for_removal();
						++score_;
					}
				}
				else if ((*i)->type() == entity_type_t::ghost) {
					auto igb = dynamic_cast<sf::RectangleShape*>((*i)->get())->getGlobalBounds();
					if (ptr->getGlobalBounds().intersects(igb) && timeout_.getElapsedTime().asSeconds() > 3) {
						--lives_;
						timeout_.restart();
					}
				}
			}
			if (lives_ == 4) ptr->setFillColor(sf::Color::Yellow);
			else if (lives_ == 3) ptr->setFillColor(sf::Color(255,165,0,255));
			else if (lives_ == 2) ptr->setFillColor(sf::Color::Red);
			else if (lives_ == 1) ptr->setFillColor(sf::Color::Magenta);
			else if (lives_ == 0) ptr->setFillColor(sf::Color::Blue);
			//
			if (ptr->getPosition().x <= -ptr->getGlobalBounds().width)
				ptr->setPosition(640, ptr->getPosition().y);
			else if (ptr->getPosition().x >= 640)
				ptr->setPosition(-ptr->getGlobalBounds().width, ptr->getPosition().y);
		}
		short get_lives() const { return lives_; }
		bool is_alive() const { return lives_ > 0; }
		unsigned get_score() const { return score_; }
	private:
		short unsigned speed_;
		short lives_;
		unsigned score_;
		sf::Clock timeout_;
};
class maze_builder_t
{
	public:
		static std::vector<entity_t*> build(std::string);
};
struct level_t : public sf::Drawable
{
	public:
		level_t(std::string path) {
			this->build(path);
			for (auto i = entities_.begin(); i != entities_.end(); ++i)
				if ((*i)->type() == entity_type_t::pacman) {
					pacman_ = dynamic_cast<pacman_t*>(*i);
					break;
				}
		}
		~level_t() {
			for (auto i = entities_.begin(); i != entities_.end(); ++i)
				delete (*i);
		}
		void update() {
			for (auto i = entities_.begin(); i != entities_.end(); ++i)
				(*i)->update(entities_);
			for (auto i = entities_.begin(); i != entities_.end();) {
				if ((*i)->is_marked_for_removal()) {
					delete (*i);
					entities_.erase(i);
				}
				else ++i;
			}
		}
		bool food_left()
		{
			return food_t::get_count() != pacman_->get_score();
		}
		pacman_t* get_pacman() { return pacman_; }
	protected:
		void build(std::string path) {
			entities_ = maze_builder_t::build(path);
		}
		void draw(sf::RenderTarget& target, sf::RenderStates states) const {
			for (auto i = entities_.begin(); i != entities_.end(); ++i)
				target.draw(**i, states);
		}
	private:
		std::vector<entity_t*> entities_;
		pacman_t* pacman_;
};
int main(int, char**)
{
	try
	{
		level_t level_001("resources/levels/001.xml");
		sf::RenderWindow window(sf::VideoMode(640,480),"App");
		sf::Event event;
		sf::Clock timer;
		sf::Font gfont; gfont.loadFromFile("resources/fonts/001.ttf");
		sf::Text gtext; gtext.setFont(gfont); gtext.setCharacterSize(30); gtext.setFillColor(sf::Color::White);
		bool game_over = false;
		bool user_won = false;

		while (window.isOpen())
		{
			while (window.pollEvent(event))
				if (event.type == sf::Event::Closed)
					window.close();
			if ( timer.getElapsedTime().asSeconds() > 1.0/60.0 ) {
				level_001.update();
				if ( !level_001.food_left() && level_001.get_pacman()->is_alive() ) {
					game_over = true;
					user_won = true;
				}
				if (! level_001.get_pacman()->is_alive() ) {
					game_over = true;
					user_won = false;
				}
				timer.restart();
			}
			window.clear();
			if (!game_over)
				window.draw(level_001);
			if (game_over)
			{
				if (user_won) {
					gtext.setString("You Win!");
				}
				else {
					auto str = std::to_string(level_001.get_pacman()->get_score());
					gtext.setString("You Lose!\nYour score: " + str);
				}
				gtext.setOrigin(gtext.getGlobalBounds().width/2, gtext.getGlobalBounds().height/2);
				gtext.setPosition(320, 240);
				window.draw(gtext);
			}
			window.display();
			if (game_over) {
				sf::sleep(sf::seconds(5));
				window.close();
			}
		}
	} catch (const std::exception& e)
	{
		std::cerr << "(ERROR): " << e.what() << std::endl;
	}
}
std::vector<entity_t*> maze_builder_t::build(std::string path)
{
	pugi::xml_document maze_doc;
	if (! maze_doc.load_file(path.c_str()) )
		throw std::domain_error{"Could not find requested file: '" + path + "'"};
	// Load maze into vector
	std::vector<std::string> maze_structure;
	for (auto node = maze_doc.first_child(); node; node = node.next_sibling()) {
		if ( std::string(node.name()) == "row" )
			maze_structure.push_back(node.child_value());
	}
	// Create entities and load them onto return vector
	std::vector<entity_t*> rvec;
	rvec.reserve(maze_structure.size() * maze_structure[0].size());
	for (unsigned i = 0; i < maze_structure.size(); ++i)
	{
		auto& row = maze_structure[i];
		// Creating immovables
		for (unsigned j = 0; j < row.size(); ++j)
		{
			if (row[j] == 'w')
			{
				// Creating wall entity
				// calculating position...
				sf::Vector2f pos(10*j, 10*i);
				// appending to container
				rvec.push_back(new wall_t(pos));
			}
			else if (row[j] == 'e')
			{
				// Creating wall entity
				// calculating position...
				sf::Vector2f pos(10*j, 10*i);
				// appending to container
				rvec.push_back(new empty_t(pos));
			}
			else if (row[j] == 'f')
			{
				sf::Vector2f pos(10*j, 10*i);
				rvec.push_back(new food_t(pos));
			}
		}
	}
	const int ghost_speed = 2;
	for (unsigned i = 0; i < maze_structure.size(); ++i)
	{
		auto& row = maze_structure[i];
		// Creating movables
		for (unsigned j = 0; j < row.size(); ++j)
		{
			if (row[j] == 'a')
			{
				sf::Vector2f pos_a(10*j, 10*i);
				auto k = i +1;
				for (; k < maze_structure.size(); ++k)
					if ( maze_structure[k][j] == 'b' ) break;	
				sf::Vector2f pos_b(10*j, 10*k);
				// appending to container
				rvec.push_back(new ghost_t(pos_a, pos_b, ghost_speed));
			}
			else if (row[j] == 'c')
			{
				sf::Vector2f pos_c(10*j, 10*i);
				auto k = j +1;
				for (; k < maze_structure[i].size(); ++k)
					if ( maze_structure[i][k] == 'd' ) break;	
				sf::Vector2f pos_d(10*k, 10*i);
				// appending to container
				rvec.push_back(new ghost_t(pos_c, pos_d, ghost_speed));
			}
			else if (row[j] == 'p')
			{
				sf::Vector2f pos(10*j, 10*i);
				rvec.push_back(new pacman_t(pos));
			}
		}
	}
	return rvec;
}
