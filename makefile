CXX=g++
CXXFLAGS=-Wall -Wextra -Wpedantic -std=c++20 -MMD

sources=$(wildcard sources/*.cpp)
objects=$(sources:.cpp=.o)
deps=$(sources:.cpp=.d)
executable=run
libraries=-lsfml-window -lsfml-graphics -lsfml-system -lsfml-audio -lsfml-network \
		  -lbox2d -lpugixml

$(executable) : CXXFLAGS += -O3
$(executable) : $(objects)
	$(CXX) -o $@ $^ $(libraries)

debug : CXXFLAGS += -O0 -g
debug : $(objects)
	$(CXX) -o $(executable) $^ $(libraries)

-include $(deps)

clean : 
	@rm $(executable) $(objects) $(deps) 2>/dev/null || true

.PHONY : debug clean
